from django.db import models

class Tag(models.Model):
    code = models.CharField(primary_key=True, max_length=255)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Post(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    tags = models.ManyToManyField(Tag)
    title = models.CharField(max_length=255)
    content = models.TextField()
    view = models.IntegerField(default=0)
    like = models.IntegerField(default=0)
    
    def __str__(self):
        return self.title




